package com.gostech.zir.service;

import com.gostech.zir.dto.AthleteRepresentationDto;
import com.gostech.zir.dto.RankRegistryDto;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@Service
public interface RankService {

    @Transactional
    List<RankRegistryDto> getWithFilters(Set<String> sports, Set<String> applicationType, Set<String> statuses,
                                         Set<String> ranks, String fullName, int pageNumber, int pageSize);

    @Transactional
    RankRegistryDto getByNumber(Long number);

    @Transactional
    AthleteRepresentationDto getAllDataByNumber(Long number);

    @Transactional
    void save(AthleteRepresentationDto dto) throws IOException;
}

package com.gostech.zir.service.impl;

import com.gostech.zir.dto.AthleteRepresentationDto;
import com.gostech.zir.dto.RankRegistryDto;
import com.gostech.zir.entity.registry.AthleteRegister;
import com.gostech.zir.repo.AthleteRegistryRepos;
import com.gostech.zir.service.RankService;
import com.gostech.zir.utils.RankMapping;
import com.gostech.zir.utils.StackTraceInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class RankServiceImpl implements RankService {

    private final EntityManager entityManager;
    private final RankMapping rankMapping;
    private final AthleteRegistryRepos athleteRegisterRepos;

    @Value(value = "${UPLOADED.FOLDER.DOCUMENT}")
    private String uploadDocument;
    @Value(value = "${UPLOADED.FOLDER.PHOTO}")
    private String uploadPhoto;

    AthleteRegister athleteRegister = new AthleteRegister();

    @Override
    @Transactional
    public List<RankRegistryDto> getWithFilters(Set<String> sports, Set<String> applicationType, Set<String> statuses,
                                                Set<String> ranks, String fullName, int pageNumber, int pageSize) {

        log.info("user name ::: called ... " + StackTraceInfo.getCurrentClassAndMethodName());

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<AthleteRegister> criteria = cb.createQuery(AthleteRegister.class);
        Root<AthleteRegister> root = criteria.from(AthleteRegister.class);

        List<Predicate> predicates = new ArrayList<>();

        if (!Objects.isNull(fullName)) {
            predicates.add(root.get("fullName").in(fullName));
        } else {
            criteria.select(root.get("fullName"));
        }

        if (CollectionUtils.isNotEmpty(sports)) {
            predicates.add(root.join("sport").get("sportName").in(sports));
        } else {
            criteria.select(root.join("sport").get("sportName"));
        }

        if (CollectionUtils.isNotEmpty(applicationType)) {
            predicates.add(root.get("applicationType").in(applicationType));
        } else {
            criteria.select(root.get("applicationType"));
        }

        if (CollectionUtils.isNotEmpty(ranks)) {
            predicates.add(root.get("rank").in(ranks));
        } else {
            criteria.select(root.get("rank"));
        }

        if (CollectionUtils.isNotEmpty(statuses)) {
            predicates.add(root.get("status").in(statuses));
        } else {
            criteria.select(root.get("status"));
        }

        List<AthleteRegister> result = entityManager.createQuery(criteria.select(root)
                        .where(cb.and(predicates.toArray(new Predicate[0]))).orderBy(cb.asc(root.get("registryNumber"))))
                .setFirstResult(pageNumber - 1)
                .setMaxResults(pageSize)
                .getResultList();

        return result.stream().map(rankMapping::mapToRankRegistryDto).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public RankRegistryDto getByNumber(Long number) {

        log.info("user name ::: called ... " + StackTraceInfo.getCurrentClassAndMethodName());

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<AthleteRegister> sportCriteria = cb.createQuery(AthleteRegister.class);
        Root<AthleteRegister> sportingEventRoot = sportCriteria.from(AthleteRegister.class);

        Predicate predicate = cb.equal(sportingEventRoot.get("registryNumber"), number);

        try {
            return rankMapping.mapToRankRegistryDto(
                    entityManager.createQuery(
                            sportCriteria.select(sportingEventRoot)
                                    .where(cb.and(predicate))).getSingleResult());
        } catch (NoResultException e) {
            log.error("No found registry number: " + e);
            return new RankRegistryDto();
        }
    }

    @Override
    @Transactional
    public AthleteRepresentationDto getAllDataByNumber(Long number) {

        log.info("user name ::: called ... " + StackTraceInfo.getCurrentClassAndMethodName());

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<AthleteRegister> sportCriteria = cb.createQuery(AthleteRegister.class);
        Root<AthleteRegister> sportingEventRoot = sportCriteria.from(AthleteRegister.class);

        Predicate predicate = cb.equal(sportingEventRoot.get("registryNumber"), number);

        try {
            return rankMapping.mapAllDataToRankRegistryDto(
                    entityManager.createQuery(
                            sportCriteria.select(sportingEventRoot)
                                    .where(cb.and(predicate))).getSingleResult());
        } catch (NoResultException e) {
            log.error("No found registry number: " + e);
            return new AthleteRepresentationDto();
        }
    }

    @Override
    @Transactional
    public void save(AthleteRepresentationDto dto) throws IOException {

        log.info("user name ::: called ... " + StackTraceInfo.getCurrentClassAndMethodName());

        try {
            athleteRegister = rankMapping.mapToAthlete(dto);
        } catch (Exception e) {
            log.error("Projection error in mapping: " + e);
        }

        uploadDocument(dto);
        uploadPhoto(dto);

        athleteRegister.setId(UUID.randomUUID());
        athleteRegister.setRegistryNumber(getRegistryNumber());
        athleteRegisterRepos.save(athleteRegister);
    }

    private void uploadDocument(AthleteRepresentationDto dto) throws IOException {
        byte[] file = Base64.getDecoder().decode(dto.getDocument().getBytes());

        Path pathToFile = Paths.get(uploadDocument, dto.getSurname() + "." + dto.getTypeOfDocument());

        try (OutputStream stream = new FileOutputStream(String.valueOf(pathToFile))) {
            stream.write(file);
            athleteRegister.setPathToDocument(String.valueOf(pathToFile));
        }
    }

    private void uploadPhoto(AthleteRepresentationDto dto) throws IOException {
        byte[] file = Base64.getDecoder().decode(dto.getPhoto().getBytes());

        Path pathToFile = Paths.get(uploadPhoto, dto.getSurname() + "." + dto.getTypeOfPhoto());

        try (OutputStream stream = new FileOutputStream(String.valueOf(pathToFile))) {
            stream.write(file);
            athleteRegister.setPathToPhoto(String.valueOf(pathToFile));
        }
    }

    private long getRegistryNumber() {
        Optional<Long> registry = athleteRegisterRepos.findMaxRegistryNumber();
        return registry.map(aLong -> aLong + 1).orElse(1L);
    }
}

package com.gostech.zir;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ConceptDataLayerApplication {



	public static void main(String[] args) {
		SpringApplication.run(ConceptDataLayerApplication.class, args);
	}


}

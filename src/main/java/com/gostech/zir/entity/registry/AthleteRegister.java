package com.gostech.zir.entity.registry;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gostech.zir.entity.directory.AllRussianRegisterOfSports;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "athlete")
public class AthleteRegister {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    private Long registryNumber;

    private String name;
    private String surname;
    private String patronymic;
    private String fullName;

    private String vdfso;

    private String placeOfStudyOrWork;
    private String homeAddress;

    private String education;
    private String sportSchool;
    private String physicalEducation;

    private String competitionName;
    private String discipline;
    private String competitionCategory;
    private String resultsShown;

    private String rank;
    private String previousRank;

    private String applicationType;
    private String status;


    private String pathToPhoto;
    private String pathToDocument;

    @Transient
    private String document;
    @Transient
    private String typeOfDocument;

    @Transient
    private String photo;
    @Transient
    private String typeOfPhoto;


    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "Europe/Moscow")
    private Date assignmentDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "Europe/Moscow")
    private Date birthDate;


    private String city;
    @Column(name = "subject_rf")
    private String subjectRF;

    private String trainerFio;
    private String trainerCategory;
    private Long experience;

    private String judgeFio;
    private String judgeCity;
    private String judgeAppointment;
    private String judgeCategory;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "sport_id")
    private AllRussianRegisterOfSports sport;

}

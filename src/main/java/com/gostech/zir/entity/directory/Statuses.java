package com.gostech.zir.entity.directory;


import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

/*
Справочник-статус
 */

@Data
@Entity
@Table(name = "statuses")
public class Statuses {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "status")
    private String status;
}

package com.gostech.zir.entity.directory;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Table(name = "sport_discipline")
@Entity
@Data
public class SportDiscipline {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "discipline_name")
    private String disciplineName;

    @ManyToMany
    private List<SportDisciplineGroup> sportDisciplineGroupList;

}
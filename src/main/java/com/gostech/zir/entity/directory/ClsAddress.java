package com.gostech.zir.entity.directory;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

/*
Справочник-адреса
 */

@Data
@Entity
@Table(name = "address")
public class ClsAddress {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "city")
    private String city;

}

package com.gostech.zir.entity.directory;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

/*
Всероссийский реестр объектов спорта
 */

@Entity
@Getter
@Setter
@Table(name = "all_russian_register_of_sports")
public class AllRussianRegisterOfSports {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "sport_name")
    private String sportName;

    @Column(name = "VRVS_code")
    private String VRVSCode;

    @ManyToOne
    @JoinColumn(name = "sports_discipline_group_id")
    private SportDisciplineGroup sportsDisciplineGroup;

    @ManyToOne
    @JoinColumn(name = "sport_discipline_id")
    private SportDiscipline sportDiscipline;

}

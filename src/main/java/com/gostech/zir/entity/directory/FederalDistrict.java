package com.gostech.zir.entity.directory;


import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

/*
Справочник-федеральных округов,
        субъектов Российской Федерации
        и муниципальных образований
 */

@Data
@Entity
@Table(name = "federal_district")
public class FederalDistrict {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "subject_RF")
    private String subjectRF;

}

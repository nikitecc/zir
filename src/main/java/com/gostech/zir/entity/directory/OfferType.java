package com.gostech.zir.entity.directory;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

/*
Справочник-тип предложения
 */


@Data
@Entity
@Table(name = "offer_type")
public class OfferType {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "offer_name")
    private String offerName;


}

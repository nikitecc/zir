package com.gostech.zir.entity.directory;


import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

/*
Справочник-пол участников
 */

@Entity
@Data
@Table(name = "gender")
public class Gender {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "gender_name")
    private String genderName;

}

package com.gostech.zir.entity.directory;


import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

/*
Справочник-Страны
 */

@Data
@Entity
@Table(name = "country")
public class Country {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "country_name")
    private String countryName;
}

package com.gostech.zir.entity.directory;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Table
@Entity
@Data
public class SportRank {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    private String rank;
}

package com.gostech.zir.entity.directory;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Table
@Entity
@Data
public class ApplicationType {

    @Id
    @GeneratedValue
    private UUID id;

    private String typeName;
}

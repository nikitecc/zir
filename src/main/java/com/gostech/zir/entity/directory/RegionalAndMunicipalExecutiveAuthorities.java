package com.gostech.zir.entity.directory;


import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

/*
Справочник - региональных и муниципальных органов
            исполнительной власти в сфере
            физической культуры и спорта
 */

@Data
@Entity
@Table(name = "directory_of_regional_and_municipal_executive_authorities ")
public class RegionalAndMunicipalExecutiveAuthorities {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "coordinating_organizations")
    private String CoordinatingOrganization;

}

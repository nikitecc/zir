package com.gostech.zir.entity.directory;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Table(name = "sport_discipline_group")
@Entity
@Data
public class SportDisciplineGroup {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "discipline_group_name")
    private String disciplineGroupName;

    @ManyToMany
    private List<SportDiscipline> sportDisciplineList;


}
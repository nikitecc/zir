package com.gostech.zir.utils;

public class StackTraceInfo {

    public static String getCurrentClassAndMethodName() {
        return Thread.currentThread().getStackTrace()[2].getClassName()
                + "." + Thread.currentThread().getStackTrace()[2].getMethodName();
    }

}

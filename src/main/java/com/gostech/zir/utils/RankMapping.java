package com.gostech.zir.utils;

import com.gostech.zir.dto.AthleteRepresentationDto;
import com.gostech.zir.dto.RankRegistryDto;
import com.gostech.zir.entity.registry.AthleteRegister;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class RankMapping {

    ModelMapper modelMapper = new ModelMapper();

    public RankRegistryDto mapToRankRegistryDto(AthleteRegister entity) {
        RankRegistryDto dto = modelMapper.map(entity, RankRegistryDto.class);

        dto.setFullName(entity.getName() + " " + entity.getSurname() + " " + entity.getPatronymic());

        return dto;
    }

    public AthleteRegister mapToAthlete(AthleteRepresentationDto dto) {
        AthleteRegister athleteRegister = modelMapper.map(dto, AthleteRegister.class);

        athleteRegister.setFullName(dto.getName() + " " + dto.getSurname() + " " + dto.getPatronymic());

        return athleteRegister;
    }

    public AthleteRepresentationDto mapAllDataToRankRegistryDto(AthleteRegister entity) {
        return modelMapper.map(entity, AthleteRepresentationDto.class);
    }

}

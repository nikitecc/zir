package com.gostech.zir.repo;

import com.gostech.zir.entity.registry.AthleteRegister;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AthleteRegistryRepos extends JpaRepository<AthleteRegister, Long> {

    @Query(value = "SELECT MAX(registry_number) FROM athlete", nativeQuery = true)
    Optional<Long> findMaxRegistryNumber();
}

package com.gostech.zir.repo;

import com.gostech.zir.entity.directory.AllRussianRegisterOfSports;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AllRussianRegisterOfSportsRepos extends JpaRepository<AllRussianRegisterOfSports, Long> {
}

package com.gostech.zir.controller;

import com.gostech.zir.dto.AthleteRepresentationDto;
import com.gostech.zir.dto.RankRegistryDto;
import com.gostech.zir.service.RankService;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/zir/rank")
@CrossOrigin
public class RankController {

    private final RankService rankService;

    @GetMapping("/filter")
    public ResponseEntity<List<RankRegistryDto>> getRegistry(
            @RequestParam(value = "sport", required = false) @Parameter(description = "Виды спорта") Set<String> sports,
            @RequestParam(value = "applicationType", required = false) @Parameter(description = "Тип заявки") Set<String> applicationType,
            @RequestParam(value = "rank", required = false) @Parameter(description = "Звание") Set<String> ranks,
            @RequestParam(value = "statuses", required = false) @Parameter(description = "Статусы") Set<String> statuses,
            @RequestParam(value = "fullName", required = false) @Parameter(description = "ФИО") String fullName,
            @RequestParam(required = false, defaultValue = "1") @Parameter(description = "Номер страницы") Integer page,
            @RequestParam(required = false, defaultValue = "10") @Parameter(description = "Количество отображаемых элементов на странице") Integer per_page) {

        return new ResponseEntity<>(rankService.getWithFilters(sports, applicationType, statuses, ranks, fullName, page, per_page), HttpStatus.OK);
    }

    @GetMapping("/{number}")
    public ResponseEntity<RankRegistryDto> getByNumber(@PathVariable("number") Long number) {

        return new ResponseEntity<>(rankService.getByNumber(number), HttpStatus.OK);
    }

    @GetMapping("all/{number}")
    public ResponseEntity<AthleteRepresentationDto> getAllDataByNumber(@PathVariable("number") Long number) {

        return new ResponseEntity<>(rankService.getAllDataByNumber(number), HttpStatus.OK);
    }

    @PostMapping(value = "/save", consumes = MediaType.ALL_VALUE)
    public ResponseEntity<Object> saveRankRegistry(@RequestBody AthleteRepresentationDto dto) throws IOException {
        rankService.save(dto);

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

}

package com.gostech.zir.dto;

import lombok.Data;

@Data
public class RankRegistryDto {
    Long registryNumber;
    String fullName;
    String sportName;
    String applicationType;
    String rank;
    String status;
}

package com.gostech.zir.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class AthleteRepresentationDto {

    String name;
    String surname;
    String patronymic;

    String rank;
    String sportName;
    String previousRank;

    String applicationType;
    String status;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "Europe/Moscow")
    Date birthDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy", timezone = "Europe/Moscow")
    Date assignmentDate;

    String education;
    String sportSchool;
    String physicalEducation;

    String city;
    String subjectRF;

    String vdfso;

    String placeOfStudyOrWork;
    String homeAddress;

    String competitionName;
    String discipline;
    String competitionCategory;
    String resultsShown;

    String trainerFio;
    String trainerCategory;
    Long experience;

    String judgeFio;
    String judgeCity;
    String judgeAppointment;
    String judgeCategory;

    String document;
    String typeOfDocument;

    String photo;
    String typeOfPhoto;
}
